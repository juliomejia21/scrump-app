<?php

namespace App\Http\Controllers;

use App\Http\Requests\BacklogItemCreateRequest;
use Illuminate\Http\Request;
use App\BacklogItem;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

//        $backlogItems = User::paginate(15);
//        $events = Events::paginate(15);
//
//        return view('adminPanel')->with(['users' => $users, 'events' => $events]);

        $backlogItems = BacklogItem::paginate(6);
//        dd($backlogItems);
        return view('home' , compact('backlogItems'));
    }

    public function save(BacklogItemCreateRequest $request) //Les 2
    {
        $data = $request->validated();
        $backlogItem = new BacklogItem();
        $backlogItem->role = $data['role'];
        $backlogItem->activity = $data['activity'];
        $backlogItem->story_points = $data['story_points'];

        $backlogItem->save();

        return redirect()->back();
    }
    public function removebli(Request $request)
    {

        $backlogItems = BacklogItem::findOrFail($request->id);
        $backlogItems->delete();

        return redirect('/home');
    }
}
