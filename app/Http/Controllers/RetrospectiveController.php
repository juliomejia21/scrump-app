<?php

namespace App\Http\Controllers;

use App\Http\Requests\RetrospectiveCreateRequest;
use App\Retrospective;
use Illuminate\Http\Request;

class RetrospectiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $retrospectives = Retrospective::all();
        return view('retrospective', compact('retrospectives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('retrospective.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $retrospective = new Retrospective;

        $retrospective->went_well = $request->went_well;
        $retrospective->went_bad = $request->went_bad;
        $retrospective->improvement = $request->improvement;
        $retrospective->save();

        return redirect()->route('retrospectives.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function show(Retrospective $retrospective)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function edit(Retrospective $retrospective)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Retrospective $retrospective)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Retrospective  $retrospective
     * @return \Illuminate\Http\Response
     */
    public function destroy(Retrospective $retrospective)
    {
        //
    }

    public function save(RetrospectiveCreateRequest $request)
    {
        $data = $request->validated();
        $retrospective = new Retrospective();
        $retrospective->went_well = $data['went_well'];
        $retrospective->went_bad = $data['went_bad'];
        $retrospective->improvement = $data['improvement'];

        $retrospective->save();

        return redirect()->back();
    }
}
