@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-2">
                <h1>Sprints</h1>
                Voeg een taak toe
            </div>
            <div class="col-6">
                {{--make new sprint items--}}
                <div class="container">
                    <div class="row mt-5 justify-content-center">
                        <div class="">
                            <form action="{{ route('planningstore') }}" method="post">
                                @csrf
                                <input type="text" name="description" placeholder="Wat doet deze nieuwe taak?" required>
                                <input type="submit" value="opslaan">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <hr>
    {{--end make sprint items--}}
    {{--view sprint items on sprint planning    --}}
    <div class="container">
        <div class="row">
            <div class="col-4" id="todo">
                <h3>To Do:</h3>
                @foreach($planning as $planningitems)
                    <div class="row">
                        <div class="card col-10">
                            <br>
                            <div class="card-header">
                                {{ $planningitems->description }}  {{ $planningitems->deadline }}
                            </div>
                            <div class="card-body text-center">
                                <button class="btn-primary">Next</button>
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#removeModal{{$planningitems->id}}">
                                    X
                                </button>
                                <div>
                                </div>
                                <div class="modal" id="removeModal{{$planningitems ->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Weet je zeker dat je <b>{{ $planningitems ->description }}</b> wil
                                                verwijderen?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Sluiten
                                                </button>
                                                <form action="{{url('deleteplanning')}}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <input type="hidden" name="id" value="{{$planningitems ->id}}">
                                                    <input type="submit" value="Verwijder" class="btn btn-danger">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                @endforeach
                <br>
            </div>
            <div class="col-4" id="progress">
                <h3>In Progress:</h3>
            </div>
            <div class="col-4" id="done">
                <h4>Done:</h4>
            </div>
        </div>
    </div>

@endsection
